import FormComponent from "./Components/FormComponentWithFormik";
import FormComponentWithYup from "./Components/FormComponentWithYup";

function App() {
  return (
    <div className="App">
      {/* <FormComponent /> */}
      <FormComponentWithYup />
    </div>
  );
}

export default App;
