import React, { Component } from 'react'
import { Formik } from 'formik';
import * as Yup from 'yup';

export class FormComponentWithYup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            info: [{}],
        }
    }

    componentDidUpdate = () => {
        console.log(this.state.info)
    }

    render() {
        return (
            <div className='max-w-[1240px] m-auto'>
                <h1 className='text-center text-4xl font-bold py-8'>Register Your Account</h1>
                <Formik
                    initialValues={{ email: '', password: '' }}

                    validationSchema={Yup.object({
                        email: Yup.string()
                            .matches(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/, "Please Enter Correct Email Address")
                            .required("Email Required"),
                        password: Yup.string()
                            .min(8, "Password Must Be At Least 8 Character")
                            .matches(/^.*(?=.{4,10})(?=.*\d)(?=.*[a-zA-Z]).*$/, "Password Must Contain Character And Number ")
                            .required("Password Required")
                    },
                    )}



                    onSubmit={(values) => {
                        this.setState({
                            info: [...this.state.info, values]
                        })
                    }}

                >
                    {({ handleChange, values, errors, touched, handleBlur, isSubmitting, handleSubmit }) => (

                        <form onSubmit={handleSubmit}>
                            <div className="mb-6">
                                <label className="block mb-2 text-sm font-medium text-gray-900">Email</label>
                                <input autoComplete='off' type="text" name='email' onBlur={handleBlur} onChange={handleChange} value={values.email} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2" />
                                <h1 className='text-red-600'>{errors.email && touched.email && errors.email}</h1>
                            </div>


                            <div className="mb-6">
                                <label class="block mb-2 text-sm font-medium text-gray-900">Password</label>
                                <input type="text" autoComplete='off' name='password' onBlur={handleBlur} onChange={handleChange} value={values.password} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" />
                                <h1 className='text-red-600'>{errors.password && touched.password && errors.password}</h1>
                            </div>

                            <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center" disabled={isSubmitting}>Submit</button>

                        </form>

                    )}
                </Formik>
            </div>
        )
    }
}

export default FormComponentWithYup